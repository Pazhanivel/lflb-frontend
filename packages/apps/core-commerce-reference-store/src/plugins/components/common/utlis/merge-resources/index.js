import * as resourceBundle from '@oracle-cx-commerce/resources';
import * as resourceBundleCustom from '@oracle-cx-commerce/core-commerce-reference-store/src/core/ui/resources';
import {buildResources} from '@oracle-cx-commerce/resources/utils';
import {merge} from '@oracle-cx-commerce/utils/generic';


/**
 * It bundles the widget specific resource keys with the supported locales from resource bundle and custom resource bundle.
 *
 * @param {*} _defaultLocale - If no Default locale Pass Empty String
 * @param {*} _CustomLocale - If no Custom locale Pass Empty String
 */

const mergeResourceBundle = (_defaultLocale, _customLocale) => {
    let _defaultLocales, _customLocales, _mergeLocales;

    (_defaultLocale && _defaultLocale !== '') ? _defaultLocales =  buildResources(resourceBundle, _defaultLocale) :  _defaultLocales = null;

    (_customLocale && _customLocale !== '') ? _customLocales = buildResources(resourceBundleCustom,_customLocale): _customLocales = null;

    if(_defaultLocales && _customLocales){
        _mergeLocales = {...merge(_defaultLocales, _customLocales)};
    } else if(_defaultLocales){
        _mergeLocales = _defaultLocales;
    } else if(_customLocales) {
        _mergeLocales = _customLocales;
    } else {
        _mergeLocales = {};
    }

    return _mergeLocales;
}

export default mergeResourceBundle;