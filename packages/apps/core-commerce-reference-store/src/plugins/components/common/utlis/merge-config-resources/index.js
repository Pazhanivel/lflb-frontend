import * as resourceBundle from '@oracle-cx-commerce/resources';
import * as resourceBundleCustom from '@oracle-cx-commerce/core-commerce-reference-store/src/core/ui/resources';
import {buildConfigResources} from '@oracle-cx-commerce/resources/utils';
import {merge} from '@oracle-cx-commerce/utils/generic';


/**
 * It bundles the widget configuration resource keys with the supported locales from resource bundle and custom resource bundle.
 *
 * @param {*} _defaultLocale - If no Default locale Pass Empty String
 * @param {*} _CustomLocale - If no Custom locale Pass Empty String
 */

const mergeConfigResourceBundle = (_defaultLocale, _customLocale) => {
    let _defaultLocales, _customLocales, _mergeLocales;

    (_defaultLocale && _defaultLocale !== '') ? _defaultLocales =  buildConfigResources(resourceBundle, _defaultLocale) :  _defaultLocales = null;

    (_customLocale && _customLocale !== '') ? _customLocales = buildConfigResources(resourceBundleCustom,_customLocale): _customLocales = null;

    if(_defaultLocales && _customLocales){
        _mergeLocales = {...merge(_defaultLocales, _customLocales)};
    } else if(_defaultLocales){
        _mergeLocales = _defaultLocales;
    } else if(_customLocales) {
        _mergeLocales = _customLocales;
    } else {
        _mergeLocales = {};
    }

    return _mergeLocales;
}

export default mergeConfigResourceBundle;