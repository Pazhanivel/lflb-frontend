/*
 ** Copyright (c) 2021 Oracle and/or its affiliates.
 */

/* eslint-disable camelcase */
/*
 ** Copyright (c) 2020 Oracle and/or its affiliates.
 */
/* eslint-disable no-undef */
const en = require('@oracle-cx-commerce/resources/en.json');
const ar = require('@oracle-cx-commerce/resources/ar.json');
const bg = require('@oracle-cx-commerce/resources/bg.json');
const ca = require('@oracle-cx-commerce/resources/ca.json');
const cs = require('@oracle-cx-commerce/resources/cs.json');
const da = require('@oracle-cx-commerce/resources/da.json');
const de = require('@oracle-cx-commerce/resources/de.json');
const el = require('@oracle-cx-commerce/resources/el.json');
const es = require('@oracle-cx-commerce/resources/es.json');
const et = require('@oracle-cx-commerce/resources/et.json');
const fi = require('@oracle-cx-commerce/resources/fi.json');
const fr = require('@oracle-cx-commerce/resources/fr.json');
const hr = require('@oracle-cx-commerce/resources/hr.json');
const hu = require('@oracle-cx-commerce/resources/hu.json');
const id = require('@oracle-cx-commerce/resources/id.json');
const it = require('@oracle-cx-commerce/resources/it.json');
const ja = require('@oracle-cx-commerce/resources/ja.json');
const ko = require('@oracle-cx-commerce/resources/ko.json');
const lt = require('@oracle-cx-commerce/resources/lt.json');
const lv = require('@oracle-cx-commerce/resources/lv.json');
const ms = require('@oracle-cx-commerce/resources/ms.json');
const nl = require('@oracle-cx-commerce/resources/nl.json');
const no = require('@oracle-cx-commerce/resources/no.json');
const pl = require('@oracle-cx-commerce/resources/pl.json');
const pt = require('@oracle-cx-commerce/resources/pt.json');
const ro = require('@oracle-cx-commerce/resources/ro.json');
const ru = require('@oracle-cx-commerce/resources/ru.json');
const sl = require('@oracle-cx-commerce/resources/sl.json');
const sr = require('@oracle-cx-commerce/resources/sr.json');
const sv = require('@oracle-cx-commerce/resources/sv.json');
const th = require('@oracle-cx-commerce/resources/th.json');
const tr = require('@oracle-cx-commerce/resources/tr.json');
const uk = require('@oracle-cx-commerce/resources/uk.json');
const vi = require('@oracle-cx-commerce/resources/vi.json');
const en_GB = require('@oracle-cx-commerce/resources/en-GB.json');
const fr_CA = require('@oracle-cx-commerce/resources/fr-CA.json');
const pt_BR = require('@oracle-cx-commerce/resources/pt-BR.json');
const sr_Latn = require('@oracle-cx-commerce/resources/sr-Latn.json');
const zh_CN = require('@oracle-cx-commerce/resources/zh-Hans.json');
const zh_TW = require('@oracle-cx-commerce/resources/zh-Hant.json');

export {
  en,
  ar,
  bg,
  ca,
  cs,
  da,
  de,
  el,
  es,
  et,
  fi,
  fr,
  hr,
  hu,
  id,
  it,
  ja,
  ko,
  lt,
  lv,
  ms,
  nl,
  no,
  pl,
  pt,
  ro,
  ru,
  sl,
  sr,
  sv,
  th,
  tr,
  uk,
  vi,
  en_GB,
  fr_CA,
  pt_BR,
  sr_Latn,
  zh_CN,
  zh_TW
};
